using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Sqlite;

namespace PartyInvites.Models
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext()
        {            
        }

        protected override void OnConfiguring(DbContextOptionsBuilder builder){
          //  builder.UseSqlite("Filename./PartyInvites.db");
            builder.UseSqlite("Data Source=PartyInvites.db");
        }

        public DbSet<GuestResponse> Invites{get;set;}
    }
}